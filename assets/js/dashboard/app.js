import './../../scss/dashboard/app.scss';

import 'angular';

import Chart from 'chart.js';
import 'angular-chart.js/dist/angular-chart.min.js';
import './app.module';
import './../angular/shared/shared.module';
import './dashboard.controller';
import './box-statistics.component';
import './dashboard.service';
import './statistics.service';
import './statistics_chart.component';
import './statistics_chart_placeholder.component';